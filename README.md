# EXOT-2018-14-recast: Workflow for QBH analysis

# Current Main Developers

Do not hesitate to contact one of the following individuals with questions:

Zoya Karpova (Zoya.Karpova@cern.ch)

# To Do

*  Add more information connects with details of event selection
*  Write steps in file steps.yml
*  Other details need for recast
*  Add text for README.md for work with this workflow

# Analysis procedure

We consider QBH-decay in to 1 lepton and 1 jet finel state. Analysis uses two approach: with ADD-model and RS1-model. Also we use model-independent study. Analysis procedure consists of following parts:

*  Strategy and method
*  Selection of events with signal signature
*  Definitions of signal, control and validation regions

# Estimation of background 

Estimation of background is going for two channels, muon and electron channels.

# Systematic uncertainties

Separately the systematic uncertainties are studied. 

# Results and interpretation

At last we get results and interpretation.

# Support Note
Support Note has more information. Support Note ATL-COM-PHYS-2018-1269 one can find here: https://cds.cern.ch/record/2637190?

# Running
Clone the workflow repository like as follows:
~~~~
https://gitlab.cern.ch/zkarpova/exot-2018-14-recast.git
~~~~

etc.
